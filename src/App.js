import React, { Component } from "react";
import { Routes, Route } from "react-router-dom";
import { Provider } from "react-redux";

import UserRegistration from "./Components/User/UserRegistration";
import Home from "./Components/Home/Home";
import UserProfile from "./Components/User/UserProfile";
import Login from "./Components/User/Login";
import EditUser from "./Components/EditUser/EditUser"
import Layout from "./Components/Layout/Layout";
import store from "./store";
import history from "./history";

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div>
          <Layout>
            <Routes history={history}>
              <Route exact path="/" element={<Home />}></Route>
              <Route
                path="/userregister"
                element={<UserRegistration />}
              ></Route>
              <Route path="/userprofile" element={<UserProfile />}></Route>
              <Route path="/login" element={<Login />}></Route>
              <Route path="/edituser" element={<EditUser />}></Route>
            </Routes>
          </Layout>
        </div>
      </Provider>
    );
  }
}

export default App;

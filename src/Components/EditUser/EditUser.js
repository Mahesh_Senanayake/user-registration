import React, { Component } from "react";
import { connect } from "react-redux";
import getAllUserId from "../../Action/allUsers";
import Card from "../Ui/Card";
import classes from "./EditUser.module.css";
import editUser from "../../Action/editUserData";

class EditUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchnic: "",
      firstname: "",
      lastname: "",
      nic: "",
      dateofbirth: "",
      email: "",
      address: "",
      gender: "",
      phonenumber: "",
      roles: "",
      id: "",
    };
  }
  editDatasubmitHandler = (event) => {
    event.preventDefault();

    const editedData = {
      firstname: this.state.firstname,
      lastname: this.state.lastname,
      nic: this.state.nic,
      dateofbirth: this.state.dateofbirth,
      email: this.state.email,
      address: this.state.address,
      gender: this.state.gender,
      phonenumber: this.state.phonenumber,
      roles: this.state.roles,
      id: JSON.parse(localStorage.getItem("EditUser"))._id,
    };

    console.log("From admin edit form", editedData);
    this.props.editUser(editedData);
  };

  searchuser = (event) => {
    event.preventDefault();
    this.props.getAllUserId(this.state.searchnic);
  };

  handleInputChange = (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value,
    });
  };

  render() {
    const data = JSON.parse(localStorage.getItem("EditUser"));
    console.log(data._id);
    if (data === null) {
      return (
        <div>
          <Card>
            <h1>Edit User Page</h1>
            <div className={classes.control}>
              <input
                type="text"
                placeholder="Search..."
                name="searchnic"
                value={this.state.value}
                onChange={this.handleInputChange}
              />
              <button className={classes.action} onClick={this.searchuser}>
                Search
              </button>
            </div>
          </Card>
        </div>
      );
    } else {
      return (
        <div>
          <h1>Profile Edit page</h1>
          <Card>
            <form
              className={classes.form}
              onSubmit={this.editDatasubmitHandler}
            >
              <div className={classes.control}>
                <input
                  type="text"
                  required
                  name="firstname"
                  placeholder={data.firstname}
                  value={this.state.value}
                  onChange={this.handleInputChange}
                />
              </div>
              <div className={classes.control}>
                <input
                  type="text"
                  required
                  name="lastname"
                  placeholder={data.lastname}
                  value={this.state.value}
                  onChange={this.handleInputChange}
                />
              </div>
              <div className={classes.control}>
                <input
                  type="text"
                  required
                  name="email"
                  placeholder={data.email}
                  value={this.state.value}
                  onChange={this.handleInputChange}
                />
              </div>
              <div className={classes.control}>
                <input
                  type="text"
                  required
                  name="address"
                  placeholder={data.address}
                  value={this.state.value}
                  onChange={this.handleInputChange}
                />
              </div>
              <div className={classes.control}>
                <input
                  type="date"
                  required
                  name="dateofbirth"
                  placeholder={data.dateofbirth}
                  value={this.state.value}
                  onChange={this.handleInputChange}
                />
              </div>
              <div className={classes.control}>
                <input
                  type="text"
                  required
                  name="nic"
                  placeholder={data.nic}
                  value={this.state.value}
                  onChange={this.handleInputChange}
                />
              </div>
              <div className={classes.control}>
                <select
                  name="gender"
                  value={this.state.value}
                  onChange={this.handleInputChange}
                >
                  <option value="">Select Gender</option>
                  <option value="male">Male</option>
                  <option value="female">Female</option>
                  <option value="other">Other</option>
                </select>
              </div>
              <div className={classes.control}>
                <input
                  type="text"
                  required
                  name="phonenumber"
                  placeholder={data.phonenumber}
                  value={this.state.value}
                  onChange={this.handleInputChange}
                />
              </div>
              <div className={classes.control}>
                <input
                  type="text"
                  required
                  name="roles"
                  placeholder={data.roles}
                  value={this.state.value}
                  onChange={this.handleInputChange}
                />
              </div>
              <div className={classes.control}>
                <button>Submit Change</button>
              </div>
            </form>
          </Card>
        </div>
      );
    }
  }
}

export default connect(null, { getAllUserId,editUser })(EditUser);

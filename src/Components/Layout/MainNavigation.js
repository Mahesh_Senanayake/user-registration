import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import classes from "./MainNavigation.module.css";
import logOut from "../../Action/userLogout";

class MainNavigation extends Component {
  render() {
    const data = JSON.parse(localStorage.getItem("UserData"));
    let content;
    if (data === null) {
      return (
        <header className={classes.header}>
          <div className={classes.logo}>
            <Link to="/">Register</Link>
          </div>
          <nav>
            <ul>
              {content}
              <li>
                <Link to="/login">Login</Link>
              </li>
              <li>
                <Link to="/userregister">Sign Up</Link>
              </li>
            </ul>
          </nav>
        </header>
      );
    } else {
      if (data.roles === "admin") {
        content = (
          <li>
            <Link to="/edituser">Edit User</Link>
          </li>
        );
      }
      return (
        <header className={classes.header}>
          <div className={classes.logo}>
            <Link to="/">Register</Link>
          </div>
          <nav>
            <ul>
              {content}
              <li>
                <Link to="/userprofile">{data.firstname}</Link>
              </li>
              <li>
                <button onClick={logOut}>Log Out</button>
              </li>
            </ul>
          </nav>
        </header>
      );
    }
  }
}

export default connect(null, { logOut })(MainNavigation);

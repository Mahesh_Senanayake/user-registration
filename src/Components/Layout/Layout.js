import React, { Component } from "react";

import classes from "./Layout.module.css";
import MainNavigation from "./MainNavigation";

class Layout extends Component {
  render() {
    return (
      <div>
        <MainNavigation />
        <main className={classes.main}>{this.props.children}</main>
      </div>
    );
  }
}

export default Layout;

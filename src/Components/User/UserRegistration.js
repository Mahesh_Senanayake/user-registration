import React, { Component } from "react";
import { connect } from "react-redux";

import Card from "../Ui/Card";
import classes from "./UserRegistration.module.css";
import userRegister from "../../Action/userAction";

class UserRegistration extends Component {
  constructor(props) {
    super(props);

    this.state = {
      firstname: "",
      lastname: "",
      nic: "",
      dateofbirth: "",
      email: "",
      address: "",
      gender: "",
      phonenumber: "",
      password: "",
      confirmpassword: "",
    };
    this.submitHandler = this.submitHandler.bind(this);
  }

  submitHandler = (event) => {
    event.preventDefault();

    const userData = {
      firstname: this.state.firstname,
      lastname: this.state.lastname,
      nic: this.state.nic,
      dateofbirth: this.state.dateofbirth,
      email: this.state.email,
      address: this.state.address,
      gender: this.state.gender,
      phonenumber: this.state.phonenumber,
      password: this.state.password,
      confirmpassword: this.state.confirmpassword,
    };

    console.log("From register form", userData);
    this.props.userRegister(userData);
  };

  handleInputChange = (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value,
    });
  };

  render() {
    return (
      <div>
        <Card>
          <form className={classes.form} onSubmit={this.submitHandler}>
            <div className={classes.control}>
              <label htmlFor="firstname">First Name</label>
              <input
                type="text"
                required
                name="firstname"
                value={this.state.value}
                onChange={this.handleInputChange}
              />
            </div>
            <div className={classes.control}>
              <label htmlFor="lastname">Last Name</label>
              <input
                type="text"
                required
                name="lastname"
                value={this.state.value}
                onChange={this.handleInputChange}
              />
            </div>
            <div className={classes.control}>
              <label htmlFor="nic">NIC</label>
              <input
                type="text"
                name="nic"
                value={this.state.value}
                onChange={this.handleInputChange}
              />
            </div>
            <div className={classes.control}>
              <label htmlFor="publisher">Date Of Birth</label>
              <input
                type="date"
                name="dateofbirth"
                value={this.state.value}
                onChange={this.handleInputChange}
              />
            </div>
            <div className={classes.control}>
              <label htmlFor="email">e-mail</label>
              <input
                type="text"
                required
                name="email"
                value={this.state.value}
                onChange={this.handleInputChange}
              />
            </div>
            <div className={classes.control}>
              <label htmlFor="phonenumber">Phone Number</label>
              <input
                type="text"
                name="phonenumber"
                value={this.state.value}
                onChange={this.handleInputChange}
              />
            </div>
            <div className={classes.control}>
              <label htmlFor="address">Address</label>
              <input
                type="text"
                name="address"
                value={this.state.value}
                onChange={this.handleInputChange}
              />
            </div>
            <div className={classes.control}>
              <label htmlFor="gender">Gender</label>
              <select
                name="gender"
                value={this.state.value}
                onChange={this.handleInputChange}
              >
                <option value="">Select Gender</option>
                <option value="male">Male</option>
                <option value="female">Female</option>
                <option value="other">Other</option>
              </select>
            </div>
            <div className={classes.control}>
              <label htmlFor="password">Password</label>
              <input
                type="password"
                required
                name="password"
                value={this.state.value}
                onChange={this.handleInputChange}
              />
            </div>
            <div className={classes.control}>
              <label htmlFor="confirmpassword">Confirm Password</label>
              <input
                type="password"
                required
                name="confirmpassword"
                value={this.state.value}
                onChange={this.handleInputChange}
              />
            </div>
            <div className={classes.control}>
              <button>Register</button>
            </div>
          </form>
        </Card>
      </div>
    );
  }
}

export default connect(null, { userRegister })(UserRegistration);

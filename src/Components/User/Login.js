import React, { Component } from "react";
import { connect } from "react-redux";

import classes from "./Login.module.css";
import Card from "../Ui/Card";
import userLogin from "../../Action/userLoginAction";

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
    };
    this.loginHandler = this.loginHandler.bind(this);
  }
  loginHandler = (event) => {
    event.preventDefault();

    const loginData = {
      email: this.state.email,
      password: this.state.password,
    };
    console.log("From register form", loginData);
    this.props.userLogin(loginData);
  };

  handleInputChange = (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value,
    });
  };

  render() {
    return (
      <div>
        <Card>
          <h1>User Login</h1>
          <form className={classes.form} onSubmit={this.loginHandler}>
            <div className={classes.control}>
              <label htmlFor="email">E mail</label>
              <input
                type="text"
                required
                name="email"
                value={this.state.value}
                onChange={this.handleInputChange}
              />
            </div>
            <div className={classes.control}>
              <label htmlFor="password">Password</label>
              <input
                type="password"
                name="password"
                value={this.state.value}
                onChange={this.handleInputChange}
              />
            </div>
            <div className={classes.control}>
              <button>Login</button>
            </div>
          </form>
        </Card>
      </div>
    );
  }
}

export default connect(null, { userLogin })(Login);

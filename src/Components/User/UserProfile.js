import React, { Component } from "react";
import { connect } from "react-redux";

import Card from "../Ui/Card";
import classes from "./UserProfile.module.css";
import editUser from "../../Action/editUserData";

class UserProfile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      firstname: "",
      lastname: "",
      nic: "",
      dateofbirth: "",
      email: "",
      address: "",
      gender: "",
      phonenumber: "",
      editmode: false,
    };
  }

  editDatasubmitHandler = (event) => {
    event.preventDefault();

    const editedData = {
      firstname: this.state.firstname,
      lastname: this.state.lastname,
      nic: this.state.nic,
      dateofbirth: this.state.dateofbirth,
      email: this.state.email,
      address: this.state.address,
      gender: this.state.gender,
      phonenumber: this.state.phonenumber,
      roles:JSON.parse(localStorage.getItem("UserData")).roles,
      id: JSON.parse(localStorage.getItem("UserData")).id,
    };

    console.log("From edit form", editedData);
    this.props.editUser(editedData);
  };

  editHandler = (e) => {
    e.preventDefault();
    this.setState({
      editmode: true,
    });
  };

  handleEditChange = (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value,
    });
  };
  render() {
    const data = JSON.parse(localStorage.getItem("UserData"));
    console.log("data fro local storage", data);
    if (this.state.editmode) {
      return (
        <div>
          <h1>Profile Edit page</h1>
          <Card>
            <form
              className={classes.form}
              onSubmit={this.editDatasubmitHandler}
            >
              <div className={classes.control}>
                <input
                  type="text"
                  required
                  name="firstname"
                  placeholder={data.firstname}
                  value={this.state.value}
                  onChange={this.handleEditChange}
                />
              </div>
              <div className={classes.control}>
                <input
                  type="text"
                  required
                  name="lastname"
                  placeholder={data.lastname}
                  value={this.state.value}
                  onChange={this.handleEditChange}
                />
              </div>
              <div className={classes.control}>
                <input
                  type="text"
                  required
                  name="email"
                  placeholder={data.email}
                  value={this.state.value}
                  onChange={this.handleEditChange}
                />
              </div>
              <div className={classes.control}>
                <input
                  type="text"
                  required
                  name="address"
                  placeholder={data.address}
                  value={this.state.value}
                  onChange={this.handleEditChange}
                />
              </div>
              <div className={classes.control}>
                <input
                  type="date"
                  required
                  name="dateofbirth"
                  placeholder={data.dateofbirth}
                  value={this.state.value}
                  onChange={this.handleEditChange}
                />
              </div>
              <div className={classes.control}>
                <input
                  type="text"
                  required
                  name="nic"
                  placeholder={data.nic}
                  value={this.state.value}
                  onChange={this.handleEditChange}
                />
              </div>
              <div className={classes.control}>
                <select
                  name="gender"
                  value={this.state.value}
                  onChange={this.handleEditChange}
                >
                  <option value="">Select Gender</option>
                  <option value="male">Male</option>
                  <option value="female">Female</option>
                  <option value="other">Other</option>
                </select>
              </div>
              <div className={classes.control}>
                <input
                  type="text"
                  required
                  name="phonenumber"
                  placeholder={data.phonenumber}
                  value={this.state.value}
                  onChange={this.handleEditChange}
                />
              </div>
              <div className={classes.control}>
                <button>Submit Change</button>
              </div>
            </form>
          </Card>
        </div>
      );
    }
    return (
      <div>
        <h1>Profile page</h1>
        <Card>
          <form className={classes.form}>
            <div className={classes.control}>
              <h1>
                {data.firstname} {data.lastname}
              </h1>
            </div>
            <div className={classes.control}>{data.address}</div>
            <div className={classes.control}>{data.email}</div>
            <div className={classes.control}>{data.dateofbirth}</div>
            <div className={classes.control}>{data.nic}</div>
            <div className={classes.control}>{data.gender}</div>
            <div className={classes.control}>{data.phonenumber}</div>
            <div className={classes.control}>
              <button onClick={this.editHandler}>Edit profile</button>
            </div>
          </form>
        </Card>
      </div>
    );
  }
}

export default connect(null, { editUser })(UserProfile);

import { API_ROOT } from "../ApiConfig";
import store from "../store";
import { USER_LOGIN_SUCCESS } from "./actionTypes";
import history from "../history";

export default function userLogin(loginData) {
  console.log("my new form object from login action", loginData);
  fetch(API_ROOT + "userlogin", {
    method: "POST",
    body: JSON.stringify(loginData),
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      console.log("Response", data);
      localStorage.clear();
      localStorage.setItem("UserData", JSON.stringify(data));
      history.push("/userprofile");
      window.location.reload();
      store.dispatch({
        type: USER_LOGIN_SUCCESS,
        payload: data,
      });
    });
}

import { ALL_USER_REQUEST } from "./actionTypes";
import { API_ROOT } from "../ApiConfig";
import store from "../store";

export default function getAllUserId(searchnic) {
  const user = JSON.parse(localStorage.getItem("UserData"));

  fetch(API_ROOT + "alluser", {
    method: "POST",
    body: JSON.stringify({ searchnic: searchnic }),
    headers: {
      "Content-Type": "application/json",
      "x-access-token": user.accessToken,
    },
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      console.log("Response", data);
      localStorage.setItem("EditUser", JSON.stringify(data));
      window.location.reload();
      store.dispatch({
        type: ALL_USER_REQUEST,
        payload: data,
      });
      console.log(data);
    });
}

/* 
let message;
      message = data.message;
      console.log("Response", message);
      history.push("/userprofile");
      window.location.reload();
      store.dispatch({
        type: EDIT_USER_REQUEST,
        payload: message,
      }); */

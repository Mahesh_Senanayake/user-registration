import { EDIT_USER_REQUEST } from "./actionTypes";
import { API_ROOT } from "../ApiConfig";
import store from "../store";
import history from "../history";

export default function editUser(editedData) {
  const user = JSON.parse(localStorage.getItem("UserData"));

  console.log("my new form object from user edit action", editedData);
  fetch(API_ROOT + "edituser", {
    method: "POST",
    body: JSON.stringify(editedData),
    headers: {
      "Content-Type": "application/json",
      "x-access-token": user.accessToken,
    },
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      let message;
      message = data.message;
      console.log("Response", message);
      history.push("/userprofile");
      window.location.reload();
      store.dispatch({
        type: EDIT_USER_REQUEST,
        payload: message,
      });
    });
}

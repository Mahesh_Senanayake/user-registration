import history from "../history";

export default function logOut() {
  localStorage.removeItem("UserData");
  history.push("/");
  window.location.reload();
}

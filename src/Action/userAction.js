import { USER_REGISTER_RESPONSE } from "./actionTypes";
import { API_ROOT } from "../ApiConfig";
import store from "../store";
import history from "../history";

export default function userRegister(userData) {
  console.log("my new form object from action", userData);
  fetch(API_ROOT + "userregister", {
    method: "POST",
    body: JSON.stringify(userData),
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      let message;
      message = data.message;
      console.log("Response", message);
      history.push("/login");
      window.location.reload();
      store.dispatch({
        type: USER_REGISTER_RESPONSE,
        payload: message,
      });
    });
}

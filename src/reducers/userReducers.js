import { USER_REGISTER_RESPONSE } from "../Action/actionTypes";

const initialState = {
  message: "",
};
const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case USER_REGISTER_RESPONSE:
      return {
        ...state,
        message: action.payload,
      };
    default:
      return state;
  }
};

export default userReducer;

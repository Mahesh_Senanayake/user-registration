import { ALL_USER_REQUEST } from "../Action/actionTypes";

const initialState = {
  allids: [],
  loadedituser:false,
};
const allUserReducers = (state = initialState, action) => {
  switch (action.type) {
    case ALL_USER_REQUEST:
      return {
        ...state,
        loadedituser:true,
        allids: action.payload,
      };
    default:
      return state;
  }
};

export default allUserReducers;

import { combineReducers } from "redux";
import userLoginReducer from "./userLoginReducer";
import userReducer from "./userReducers";
import allUserReducers from './alluserReducers'

export default combineReducers({
  userLoginReducer,
  userReducer,
  allUserReducers,
});

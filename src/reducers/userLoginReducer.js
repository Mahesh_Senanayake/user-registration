import { USER_LOGIN_SUCCESS } from "../Action/actionTypes";

const initialState = {
  userData: {},
};
const userLoginReducer = (state = initialState, action) => {
  switch (action.type) {
    case USER_LOGIN_SUCCESS:
      return {
        ...state,
        userData: action.payload,
      };
    default:
      return state;
  }
};

export default userLoginReducer;
